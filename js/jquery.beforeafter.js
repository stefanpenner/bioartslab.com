/*
 * jQuery beforeafter plugin
 * @author admin@catchmyfame.com - http://www.catchmyfame.com
 * @version 1.0
 * @date June 22, 2009
 * @category jQuery plugin
 * @copyright (c) 2009 admin@catchmyfame.com (www.catchmyfame.com)
 * @license CC Attribution-No Derivative Works 3.0 - http://creativecommons.org/licenses/by-nd/3.0/
 */
// before image : $('div:eq(2)', that)
// after image  : $('div:eq(3)', that)
(function($){
	$.fn.extend({ 
		beforeAfter: function(options)
		{
			var defaults = { animateIntro  : true,
			                 introDelay    : 1000,
				               introDuration : 1000,
				               showFullLinks : true },

		      options = $.extend(defaults, options),
          randID  = Math.round(Math.random()*100000000);
    	return this.each(function() {
			  var o      = options,
			      that   = $(this),
            first  = $('img:first', that),
            last   = $('img:last',  that),
            firstWidth  = first.width(),
			      firstHeight = first.height(),
            firstSrc    = first.attr('src'),
            image1 = $('<img />').attr('src', firstSrc),
			      image2 = $('<img />').attr('src', firstSrc);
			
			    that
			      .width(firstWidth)
			      .height(firstHeight)
			      .css({'overflow':'hidden',
                  'position':'relative',
                  'padding':'0'});
			
			// Preload images and assign them IDs
			
			first.attr('id','beforeimage' + randID);
			last.attr('id', 'afterimage'  + randID);
			
			$('div', that).css('float', 'left'); // Float all divs within the container left
			
			// Create an inner div wrapper (dragwrapper) to hold the images. Create drag handle
			that.prepend('<div id="dragwrapper'+randID+'"><div id="drag'+randID+'"><img width="8" height="56" alt="handle" src="/images/handle.gif" title="Drag me left or right to see the before and after images" id="handle'+randID+'" /></div></div>');
			$('#dragwrapper'+randID)
        .css({'position':'absolute',
              'padding' :'0',
              'left'    :(firstWidth/2)-($('#handle'+randID).width()/2)+'px',
              'z-index' :'20',
              'opacity' :.25})
        .width($('#handle'+randID).width())
        .height(firstHeight);
			
      var before = $('div:eq(2)',that),
          after  = $('div:eq(3)',that);

      before
        .height(firstHeight)
        .width(firstWidth/2)
        .css({'position':'absolute',
              'overflow':'hidden',
              'left'    :'0px',
              'z-index' :'10'}); // Set CSS properties of the before image div
        
      after
        .height(firstHeight)
        .width(firstWidth)
        .css({'position':'absolute',
              'overflow':'hidden',
              'right'   :'0px'});	// Set CSS properties of the after image div

			$('#drag'+randID)
        .width(2)
        .height(firstHeight)
        .css({'background':'#888',
              'position'  :'absolute',
              'left'      :'3px'});	// Set drag handle CSS properties

			var image_css = { 'position':'absolute',
                        'top'     :'0px',
                        'left'    :'0px'};

      $('#beforeimage'+randID).css(image_css);
			$('#afterimage' +randID).css(image_css);

			$('#handle'+randID)
        .css({'position':'relative',
              'cursor':'pointer',
              'top':(firstHeight/2)-($('#handle'+randID).height()/2)+'px',
              'left':'-3px'});
			
			that.append('<img src="/images/lt-small.png" width="7" height="15" id="lt-arrow'+randID+'"><img src="/images/rt-small.png" width="7" height="15" id="rt-arrow'+randID+'">');

			if(o.showFullLinks){	
				that.after('<div class="balinks" id="links'+randID+'" style="position:relative"><span class="bflinks"><a id="showleft'+randID+'" href="javascript:void(0)">Show before</a></span><span class="bflinks"><a id="showright'+randID+'" href="javascript:void(0)">Show after</a></span></div>');

				$('#links'+randID).width(firstWidth);

				$('#showleft'+randID)
          .css({'position':'relative','left':'0px'})
          .click(function(){
					before.animate({ width : firstWidth},200);
					$('#dragwrapper'+randID).animate({left:firstWidth-$('#dragwrapper'+randID).width()+'px'},200);
				});

				$('#showright'+randID)
          .css({'position':'absolute','right':'0px'})
          .click(function(){
					before.animate({width:0},200);
					$('#dragwrapper'+randID).animate({left:'0px'},200);
				});
			}

			var barOffset = $('#dragwrapper'+randID).position(), // The coordinates of the dragwrapper div
			    startBarOffset     = barOffset.left, // The left coordinate of the dragwrapper div
			    originalLeftWidth  = before.width(),
			    originalRightWidth = after.width();

			$('#dragwrapper'+randID)
        .draggable({ handle: $('#handle'+randID),
                     containment:that,
                     axis:'x',
                     drag: function(e, ui){

				  var offset = $(this).position();
				      barPosition = offset.left - startBarOffset;

				      before.width(originalLeftWidth + barPosition);

				  $('#lt-arrow'+randID)
            .stop()
            .animate({opacity:0},50);

				  $('#rt-arrow'+randID)
            .stop()
            .animate({opacity:0},50);
				  }
			});

			if(o.animateIntro)
			{
				before.width(firstWidth);
				$('#dragwrapper'+randID).css('left',firstWidth-($('#dragwrapper'+randID).width()/2)+'px');
				
        setTimeout(function(){
					$('#dragwrapper'+randID)
            .css({'opacity':1})
            .animate({'left':(firstWidth/2)-($('#dragwrapper'+randID).width()/2)+'px'},o.introDuration, function(){
              $('#dragwrapper'+randID)
                .animate({'opacity':.25},1000)
            });

					// The callback function at the end of the last line is there because Chrome seems to forget that the divs have overlay  hidden applied earlier
					before
            .width(firstWidth)
            .animate({'width':firstWidth/2+'px'},o.introDuration,function(){
              before
                .css('overflow','hidden');
              clickit();
            });
				},o.introDelay);
			}else{
				clickit();
			}

			function clickit(){
				that.hover(function(){
          var rightArrow = $('#rt-arrow'+randID),
              leftArrow  = $('#lt-arrow'+randID),
              dragWrapper = parseInt($('#dragwrapper'+randID).css('left'));

					leftArrow
            .stop()
            .css({'z-index' :'20',
                  'position':'absolute',
                  'top':firstHeight/2-leftArrow.height()/2+'px',
                  'left':dragWrapper-10+'px'})
            .animate({opacity:1,
                      left:parseInt(leftArrow.css('left'))-6+'px'},500);

						rightArrow
              .stop()
              .css({'position':'absolute',
                    'top':firstHeight/2-leftArrow.height()/2+'px',
                    'left':dragWrapper+10+'px'})
              .animate({opacity:1,
                        left:parseInt(rightArrow.css('left'))+6+'px'},500);

						$('#dragwrapper'+randID).animate({'opacity':1},500);

					},function(){
            var rightArrow = $('#rt-arrow'+randID),
                leftArrow  = $('#lt-arrow'+randID),
                lt = parseInt(rightArrow.css('left')),
                rt = parseInt(leftArrow.css('left'));

            rightArrow.animate({opacity:0,left:lt+6+'px'},500);
            leftArrow.animate({opacity:0,left:rt-6+'px'},500);

						$('#dragwrapper'+randID)
              .animate({'opacity':.25},500);
					}
				);

				// When clicking in the container, move the bar and imageholder divs
				that.click(function(e){
					
					var clickX = e.pageX - $(this).offset().left;
          
					$('#dragwrapper'+randID)
            .stop()
            .animate({'left':clickX-($('#dragwrapper'+randID).width()/2)+'px'},600);

					before
            .stop() 
            .animate({'width':clickX+'px'},600,function(){
              before
                .css('overflow','hidden');
            }); // webkit fix for forgotten overflow

					$('#lt-arrow'+randID)
            .stop()
            .animate({opacity:0},50);

					$('#rt-arrow'+randID)
            .stop()
            .animate({opacity:0},50);

				});
				
        that
          .one('mousemove', function(){
            $('#dragwrapper'+randID)
              .stop()
              .animate({'opacity':1},500);
          }); // If the mouse is over the container and we animate the intro, we run this to change the opacity since the hover event doesnt get triggered yet
			  }
  		});
    }
	});
})(jQuery);

